from django import forms
from django.db.models import fields

from lab_1.models import Friend

class Friendform (forms.ModelForm) :
    class Meta:
        model = Friend
        fields = "__all__"