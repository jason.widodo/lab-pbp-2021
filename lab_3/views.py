from django import forms
from django.shortcuts import redirect, render
from datetime import datetime, date
from django.http import HttpResponseRedirect, response
from django.urls import reverse
from lab_1.models import Friend
from .forms import Friendform
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)


@login_required(login_url='/admin/login/')
def add_friend(request):
    TampFriend = Friendform(request.POST or None)
    response = {'form' : TampFriend}
    if request.method == "POST":
        if TampFriend.is_valid():
            TampFriend.save()
            return redirect('index1')
    return render(request, 'lab3_form.html', response)
