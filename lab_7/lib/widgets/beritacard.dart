import 'package:flutter/material.dart';
import 'package:lab_6/models/berita.dart';

class beritacard extends StatelessWidget {
  final Berita berita;
  const beritacard(this.berita, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 4),
        child: Card(
          clipBehavior: Clip.antiAlias,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ExpansionTile(
              backgroundColor: Colors.white,
              // Judul
              title: Text(
                berita.judulBerita,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              trailing: const SizedBox(),
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                      top: 16.0, left: 16.0, right: 16.0, bottom: 8.0),
                  child: Row(
                    children: [
                      Flexible(
                        child: Text(
                          'Dibuat oleh ' + berita.penulis,
                          style:
                              TextStyle(color: Colors.black.withOpacity(0.6)),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 16.0, left: 16.0, right: 16.0, bottom: 8.0),
                  child: Row(
                    children: [
                      Flexible(
                        child: Text(
                          'Isi Berita : ' + berita.isiBerita,
                          style:
                              TextStyle(color: Colors.black.withOpacity(0.6)),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
