import 'package:lab_6/models/berita.dart';
import 'package:lab_6/models/berita_json.dart';

// ignore: non_constant_identifier_names
BeritaJson DUMMY_CATEGORIES = BeritaJson(
  model: 'berita.berita',
  pk: 1,
  fields: [
    Berita(
      penulis: "Bruceaaa.",
      judulBerita: 'Berita 1',
      isiBerita: 'pekerjaan,teman,pendidikan',
    ),
    Berita(
      penulis: "ADASDSDA",
      judulBerita: 'Berita 2',
      isiBerita: 'hidup sehat',
    ),
    Berita(
      penulis: "Hisdasdtan",
      judulBerita: 'Berita 3',
      isiBerita: 'aaaaaahhhhh',
    )
  ],
);
