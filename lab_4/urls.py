from django.urls import path
from .views import add_notes, index, note_list

urlpatterns = [
    path('', index, name='index'),
    path('addNote', add_notes, name='add_notes'),
    path('note_list', note_list, name='note_list'),

]
