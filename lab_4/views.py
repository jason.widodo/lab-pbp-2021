from django.shortcuts import render, redirect
from .models import Note
from lab_4.forms import Noteform
from django.http.response import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request):
    NoteD = Note.objects.all() 
    response = {'note': NoteD}
    return render(request, 'lab4_index.html', response)

@login_required(login_url='/admin/login/')
def add_notes(request):
    TulisanNotes = Noteform(request.POST or None)
    response = {'form' : TulisanNotes}
    if request.method == "POST":
        if TulisanNotes.is_valid():
            TulisanNotes.save()
            return redirect('/lab-4')
    return render(request, 'lab4_form.html', response)

def note_list(request):
    Notes = Note.objects.all() 
    response = {'notes': Notes}
    return render(request, 'lab4_note_list.html', response)
