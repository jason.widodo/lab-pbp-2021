from django import forms
from django.db.models import fields

from lab_2.models import Note

class Noteform (forms.ModelForm) :
    class Meta:
        model = Note
        fields = "__all__"