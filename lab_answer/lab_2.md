Apakah perbedaan antara JSON dan XML?

- JSON beorientasi pada data sedangkan XML pada dokumen
- JSON mendukung array sedangkan XML tidak
- file JSON diakhiri dengan ekstensi .json sedangkan XML .xml

- berdasarkan tampilan yang dihasilkan, JSON terlihat lebih simple karena hanya menampilkan data-datanya saja tanpa tag dalam bentuk array (key : value). Namun, secara pribadi saya merasa JSON lebih sulit dibaca jika arraynya berbentuk memanjang kesamping dan tidak rapi. Berbeda dengan XML yang lebih rumit karena harus menampilkan tag tiap data yang ada.

Apakah perbedaan antara HTML dan XML?

- HTML tidak peka dengan sensitif case sedangkan XML iya
- HTML berfokus pada tampilan data sedangkan XML berfokus pada penyampaian informasi
- HTML berfungsi untuk mengembangkan struktur halaman web sedangkan XML membantu bertukar data di berbagai platform

- berdasarkan tampilan yang dihasilkan, HTML dapat mempercantik tampilan data dan tidak menampilkan tag-tag yang digunakan. Sedangkan XML menampilkan struktur data yang dibungkus oleh tag dan tidak memperlukan style. tampilan XML berbentuk seperti tree yang dijabarkan dari bagian paling luar.



Jason Widodo - 2006596415
PBP A