import 'dart:convert';

import 'package:lab_6/models/berita.dart';

class BeritaJson {
  // pk -> primary key from serialization

  BeritaJson({
    required this.model,
    required this.pk,
    required this.fields,
  });

  final String model;
  final int pk;
  final List<Berita> fields;

  factory BeritaJson.fromJson(String str) =>
      BeritaJson.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory BeritaJson.fromMap(Map<String, dynamic> json) => BeritaJson(
        model: json["model"],
        pk: json["pk"],
        fields: List<Berita>.from(json["fields"].map((x) => x)),
      );

  Map<String, dynamic> toMap() => {
        "model": model,
        "pk": pk,
        "fields": List<dynamic>.from(fields.map((x) => x)),
      };
}
