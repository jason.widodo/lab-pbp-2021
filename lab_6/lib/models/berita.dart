import 'package:flutter/foundation.dart';
import 'dart:convert';
// To parse this JSON data, do
//
//     final Pojok_Curhat = Pojok_CurhatFromMap(jsonString);

class Berita {
  Berita({
    required this.judulBerita,
    //required this.tanggalRilis,
    required this.penulis,
    //required this.spoiler,
    required this.isiBerita,
  });

  final String judulBerita;
  //final String tanggalRilis;
  final String penulis;
  //final String spoiler;
  final String isiBerita;

  factory Berita.fromJson(String str) => Berita.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Berita.fromMap(Map<String, dynamic> json) => Berita(
        judulBerita: json["judulBerita"],
        //tanggalRilis: json["tanggalRilis"],
        penulis: json["penulis"],
        //spoiler: json["spoiler"],
        isiBerita: json["isiBerita"],
      );

  Map<String, dynamic> toMap() => {
        "judulBerita": judulBerita,
        //"tanggalRilis": tanggalRilis,
        "penulis": penulis,
        //"spoiler": spoiler,
        "isiBerita": isiBerita,
      };
}
