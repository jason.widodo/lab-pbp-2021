import 'package:flutter/material.dart';
import 'package:lab_6/dummy_data.dart';
import 'package:lab_6/models/berita.dart';
import 'package:lab_6/screens/add_berita.dart';
import 'package:lab_6/widgets/beritacard.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => beritahomePagestate();
}

class beritahomePagestate extends State<HomePage> {
  List<Berita> dummyBerita = DUMMY_CATEGORIES.fields;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Berita'),
      ),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            // ignore: sized_box_for_whitespace
            Container(
              height: 64,
              child: const DrawerHeader(
                child: Text(
                  'reflekt.io',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                  ),
                ),
                decoration: BoxDecoration(
                  color: Color(0xFF24262A),
                ),
              ),
            ),
            ListTile(
              title: const Text('Home'),
              onTap: () {
                // Go to Riwayat Curhat screen
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Masukan Berita Baru'),
              onTap: () {
                // Go to Jurnal Baru page
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const addBeritaPage(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            dummyBerita.isEmpty
                ? const Center(
                    child: Padding(
                      padding: EdgeInsets.only(top: 30.0),
                      child: Text(
                        'Tekan tombol tambah untuk menambahkan jurnal baru.',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                : ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: dummyBerita.length,
                    itemBuilder: (context, index) {
                      return beritacard(dummyBerita[index]);
                    },
                  ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color(0xFF0B36A8),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const addBeritaPage()));
        },
        tooltip: 'Berita Baru',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
